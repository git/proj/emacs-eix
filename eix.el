;;; eix.el --- Eix integration -*- lexical-binding: t -*-


;; Copyright 2022 Gentoo Authors


;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;; Author: Maciej Barć <xgqt@riseup.net>
;; Homepage: https://gitweb.gentoo.org/proj/emacs-eix.git
;; Keywords: processes
;; Maintainer: <emacs@gentoo.org>
;; Package-Requires: ((emacs "24.3"))
;; Version: 0.0.0



;;; Commentary:


;; Eix integration for Emacs.
;; Interface with the "eix" Gentoo tool from within GNU Emacs.

;; Eix resources:
;; - repository: https://github.com/vaeth/eix
;; - Gentoo Wiki page: https://wiki.gentoo.org/wiki/Eix


;; Potential improvement / alternative:
;; Eix provides output in XML format with the "--xml" switch.
;; Maybe this way we can customize the output more or get more info.


;; This library was originally written as part of "emacs-gentoo"
;; by Maciej Barć.
;; It was later relicensed by the author under the GPL-2-or-later license
;; and republished under the Gentoo GNU Emacs project.

;; Original repository: https://gitlab.com/xgqt/emacs-gentoo



;;; Code:


(require 'shell)


(defconst eix-version "0.0.0"
  "Emacs-Eix version.")

(defgroup eix nil
  "Eix integration."
  :group 'external)


;; Executables

(defcustom eix-command (executable-find "eix")
  "Path to the \"eix\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)

(defcustom eix-diff-command (concat eix-command "-diff")
  "Path to the \"eix-diff\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)

(defcustom eix-update-command (concat eix-command "-update")
  "Path to the \"eix-update\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)

(defcustom eix-remote-command (concat eix-command "-remote")
  "Path to the \"eix-remote\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)

(defcustom eix-sync-command (concat eix-command "-sync")
  "Path to the \"eix-sync\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)

(defcustom eix-test-command (concat eix-command "-test-obsolete")
  "Path to the \"eix-test-obsolete\" binary."
  :safe 'stringp
  :type 'file
  :group 'eix)


;; Helpers

(defun eix--execute-command (&rest args)
  "Execute a command constructed of ARGS in the EIX buffer."
  (let ((eix-buffer-name "*EIX*")
        (eix-error-buffer-name "*EIX ERRORS*")
        (cmd (apply 'concat (mapcar (lambda (s) (concat " " s)) args)))
        (process-environment (append process-environment '("EIX_LIMIT=0"))))
    (let ((eix-buffer (get-buffer-create eix-buffer-name)))
      (with-current-buffer eix-buffer
        (async-shell-command cmd eix-buffer-name eix-error-buffer-name)
        (eix-browse-mode)))))

;; TODO: local only?

(defun eix--get-all-local-packages ()
  "Gel all available packages in local repositories.
Used primarily for `completing-read' in `eix-search-exact'"
  (let ((process-environment (append process-environment '("EIX_LIMIT=0"))))
    (split-string
     (shell-command-to-string (concat eix-command " --only-names")) "\n" t)))


;; Mode

(defcustom eix-browse-mode-hook nil
  "Hook for `eix-browse' major mode."
  :type 'hook
  :group 'eix)

(defconst eix-browse-font-lock-keywords
  '(("->" . 'font-lock-keyword-face)  ; arrow
    (" [!<=>]+" . 'font-lock-variable-name-face)  ; !>=cat/pkg
    (" [+-][0-9a-z-]+" . 'font-lock-constant-face)  ; +z3 -qt5
    ("xpak" . 'font-lock-comment-face))
  "Font-lock keywords for `eix-browse' major mode.")

(defvar eix-browse-mode-map
  (let ((eix-browse-mode-map (make-keymap)))
    (define-key eix-browse-mode-map (kbd "/") 'isearch-forward)
    (define-key eix-browse-mode-map (kbd "?") 'describe-mode)
    (define-key eix-browse-mode-map (kbd "h") 'describe-mode)
    (define-key eix-browse-mode-map (kbd "q") 'quit-window)
    (define-key eix-browse-mode-map (kbd "r") 'isearch-backward)
    (define-key eix-browse-mode-map (kbd "s") 'isearch-forward)
    eix-browse-mode-map)
  "Key map for `eix-browse' major mode.")

(define-derived-mode eix-browse-mode shell-mode "eix-browse"
  "Major mode for browsing \"eix\" command's output.
Do not use anywhere else."
  (run-hooks 'eix-browse-mode-hook)
  (use-local-map eix-browse-mode-map)
  (setq font-lock-defaults '(eix-browse-font-lock-keywords))
  (goto-address-mode)
  (setq buffer-read-only t))


;; Main provided features

;; TODO: add call options? e.g.: -R, -O, ...

;;;###autoload
(defun eix-diff ()
  "Show eix cache differences."
  (interactive)
  (eix--execute-command eix-diff-command))

;;;###autoload
(defun eix-local-update ()
  "Update eix database of local packages."
  (interactive)
  (eix--execute-command eix-update-command))

;;;###autoload
(defun eix-remote-update ()
  "Update eix database of remote packages."
  (interactive)
  (eix--execute-command eix-remote-command "update"))

;; TODO: (eix-search) completing-read of --verbose --in-overlay ?

;;;###autoload
(defun eix-search (&optional package)
  "Search for a PACKAGE."
  (interactive)
  (let ((pkg (if package package (read-string "Enter package name: "))))
    (eix--execute-command eix-command "--verbose" pkg)))

;;;###autoload
(defun eix-search-exact ()
  "Search for a package using completion of available packages."
  (interactive)
  (let ((package (completing-read "Package: " (eix--get-all-local-packages))))
    (eix-search package)))

;;;###autoload
(defun eix-sync ()
  "Synchronize package repositories."
  (interactive)
  (eix--execute-command eix-sync-command))

;;;###autoload
(defun eix-test ()
  "Test for obsolete packages."
  (interactive)
  (eix--execute-command eix-test-command))

;;;###autoload
(defun eix-show-updates ()
  "List packages that can be updated."
  (interactive)
  (eix--execute-command eix-command "--compact" "--upgrade"))


(provide 'eix)



;;; eix.el ends here
